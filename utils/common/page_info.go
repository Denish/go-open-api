package common

import "math"

// ListResponse 通用分页结构
type ListResponse struct {
	List     interface{} `json:"list"`
	PageInfo PageInfo    `json:"page_info"`
}

// PageInfo 通用分页信息
type PageInfo struct {
	Page       int   `json:"page"`
	Size       int   `json:"size"`
	TotalCount int64 `json:"total_count"`
	TotalPage  int   `json:"total_page"`
}

// GetPageInfo 获取分页信息
func GetPageInfo(page, size int, total int64) PageInfo {
	return PageInfo{
		Page:       page,
		Size:       size,
		TotalCount: total,
		TotalPage:  int(math.Ceil(float64(total) / float64(size))),
	}
}

func GetLimit(page, size int) (int, int) {
	return size, size * (page - 1)
}

func SlicePage(page, pageSize, nums int) (start, end int) {
	if page < 0 {
		page = 1
	}
	if pageSize < 0 {
		pageSize = 20
	}

	if pageSize > nums {
		return 0, nums
	}
	// 总页数
	pageCount := int(math.Ceil(float64(nums) / float64(pageSize)))
	if page > pageCount {
		return 0, 0
	}
	start = (page - 1) * pageSize
	end = start + pageSize

	if end > nums {
		end = nums
	}
	return start, end
}

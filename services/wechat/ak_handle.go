package wechat_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/silenceper/wechat/v2/cache"
	"github.com/silenceper/wechat/v2/credential"
	wechatUtil "github.com/silenceper/wechat/v2/util"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

// CkAccessToken 默认AccessToken 获取
type CkAccessToken struct {
	appID           string
	appSecret       string
	cacheKeyPrefix  string
	cache           cache.Cache
	accessTokenLock *sync.Mutex
}

// NewCkAccessToken new CkAccessToken
func NewCkAccessToken(appID, appSecret, cacheKeyPrefix string, cache cache.Cache) *CkAccessToken {
	if cache == nil {
		panic("cache is ineed")
	}
	return &CkAccessToken{
		appID:           appID,
		appSecret:       appSecret,
		cache:           cache,
		cacheKeyPrefix:  cacheKeyPrefix,
		accessTokenLock: new(sync.Mutex),
	}
}

// GetAccessToken 获取access_token,先从cache中获取，没有则从服务端获取
func (ak *CkAccessToken) GetAccessToken() (accessToken string, err error) {
	// 先从cache中取
	accessTokenCacheKey := fmt.Sprintf("%s:access_token:%s", ak.cacheKeyPrefix, ak.appID)
	if val := ak.cache.Get(accessTokenCacheKey); val != nil {
		return val.(string), nil
	}

	// 加上lock，是为了防止在并发获取token时，cache刚好失效，导致从微信服务器上获取到不同token
	ak.accessTokenLock.Lock()
	defer ak.accessTokenLock.Unlock()

	// 双检，防止重复从微信服务器获取
	if val := ak.cache.Get(accessTokenCacheKey); val != nil {
		return val.(string), nil
	}

	// cache失效，从微信服务器获取
	var resAccessToken credential.ResAccessToken
	resAccessToken, err = GetTokenFromServer(ak.appID, ak.appSecret)
	if err != nil {
		return
	}

	expires := resAccessToken.ExpiresIn - 1500
	err = ak.cache.Set(accessTokenCacheKey, resAccessToken.AccessToken, time.Duration(expires)*time.Second)
	if err != nil {
		return
	}
	accessToken = resAccessToken.AccessToken
	return
}

// GetTokenFromServer 强制从微信服务器获取token
func GetTokenFromServer(appID, appSecret string) (resAccessToken credential.ResAccessToken, err error) {
	token, err := GetWxToken(appID, appSecret)
	if err != nil {
		return
	}
	resAccessToken = credential.ResAccessToken{
		CommonError: wechatUtil.CommonError{},
		AccessToken: token,
		ExpiresIn:   7200,
	}
	return
}

var CENTER_API_TOKEN_URL = "https://cds-center-api.limayao.com/wechat/token"

const (
	CENTER_APIHOST = "https://cds-center-api.limayao.com"
)

type CommonRet struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func GetWxToken(appid, appsecret string) (string, error) {
	url := fmt.Sprintf(CENTER_API_TOKEN_URL+"?appid=%s&appsecret=%s", appid, appsecret)
	data, err := HttpGetWithTimeout(url, 3*time.Second)
	if err != nil {
		return "", err
	}
	var ret CommonRet
	err = json.Unmarshal(data, &ret)
	if err != nil {
		return "", err
	}
	if ret.Code == 0 {
		if token, ok := ret.Data.(string); ok {
			return token, nil
		} else {
			return "", errors.New("获取token失败")
		}
	} else {
		return "", errors.New(ret.Msg)
	}
}

func HttpGetWithTimeout(url string, timeout time.Duration, headers ...map[string]string) ([]byte, error) {
	client := &http.Client{
		Timeout: timeout,
	}
	req, err := http.NewRequest("GET", url, nil) //建立一个请求
	if err != nil {
		return nil, err
	}
	if len(headers) > 0 {
		header := headers[0]
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}
	resp, err := client.Do(req) //提交
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

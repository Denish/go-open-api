package common

import common1 "niu-ren/utils/common"

type DataPermission struct {
	DataScope int
	CorpId    int
	UserId    int
	DeptId    int
	RoleId    int
	MyUserId  []int `gorm:"-"  json:"userId"`
}

func CheckIsAdmin(rid int) bool {
	if rid == 1 || rid == 2 {
		return true
	}
	return false
}

func CheckIsManage(rid int) bool {
	if rid == 1 || rid == 2 || rid == 3 || rid == 4 {
		return true
	}
	return false
}

// CheckIsClassify 带货和内容区分
func CheckIsClassify(rid, class int) bool {
	if class == 1 { //带货
		if rid == 3 || rid == 5 {
			return false
		}
	} else { //内容
		if rid == 4 || rid == 6 {
			return false
		}
	}
	return true
}

//CheckSelfStaff 操作人,目标用户
func CheckSelfStaff(p *DataPermission, uid int) bool {
	if CheckIsAdmin(p.RoleId) {
		return true
	}
	if common1.InArrayInt(uid, p.MyUserId) {
		return true
	}
	return false
}

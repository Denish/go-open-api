package common

import (
	"encoding/json"
	redis2 "go-beego-api/app/models/redis"
	"go-beego-api/component/redis"
	"go-beego-api/exception"
	"go-beego-api/utils/common"
)

// Repeat 请求频繁
func Repeat(method string, data interface{}, expTime int) (err error) {
	dataStr, _ := json.Marshal(data)
	str := method + string(dataStr)
	md5Str := common.Md5Encode(str)

	//限制请求
	times, _ := redis.Redis().TTL(redis2.GetRepeatKey(md5Str))
	if times > 0 {
		err = exception.DoNotRepeat
		return
	}
	redis.Redis().SetString(redis2.GetRepeatKey(md5Str), str, expTime)
	return
}

package redis

import (
	"github.com/gomodule/redigo/redis"
	"sync"
	"time"
)

type RClient struct {
	db         string
	clientPool *redis.Pool
}

func (rc *RClient) getPool() *redis.Pool {
	if rc.clientPool != nil {
		return rc.clientPool
	} else {
		return nil
	}
}

func (rc *RClient) SetPool(db string, p *redis.Pool) {
	rc.clientPool = p
	rc.db = db
}

func (rc *RClient) Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do(commandName, args...)
		return v, err
	} else {
		return nil, client.Err()
	}
}

// GetInt 这边会有个redis.ErrNil,是string里抛出的，不是redis问题
func (rc *RClient) GetInt(key string) (int, error) {

	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("GET", key)
		val, err := redis.Int(v, err)
		return val, err
	} else {
		return 0, client.Err()
	}

}

func (rc *RClient) HGetInt(key string, subKey string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("HGET", key, subKey)
		val, _ := redis.Int(v, err)
		return val, err
	} else {
		return 0, client.Err()
	}

}

func (rc *RClient) MGetInt(keys []string) ([]int, error) {
	if len(keys) == 0 {
		return []int{}, nil
	}
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		var args []interface{}
		for _, v := range keys {
			args = append(args, v)
		}
		v, err := client.Do("MGET", args...)
		val, _ := redis.Ints(v, err)
		return val, err
	} else {
		return []int{}, client.Err()
	}
}

func (rc *RClient) Incr(key string, expire int) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("INCR", key)
		val, _ := redis.Int(v, err)
		if val <= 1 && expire != -1 {
			client.Do("EXPIRE", key, expire)
		}
		return val, nil
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) IncrBy(key string, add, expire int) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("INCRBY", key, add)
		val, _ := redis.Int(v, err)
		if val == add && expire != -1 {
			client.Do("EXPIRE", key, expire)
		}
		return val, nil
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) DecrBy(key string, num int) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("DECRBY", key, num)
		val, _ := redis.Int(v, err)
		return val, nil
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) Decr(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("DECR", key)
		val, _ := redis.Int(v, err)
		return val, nil
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) IncrKeys(keys []string, expire int) error {

	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		for _, key := range keys {
			v, err := client.Do("INCR", key)
			val, _ := redis.Int(v, err)
			if val <= 1 {
				client.Do("EXPIRE", key, expire)
			}
		}
		return nil
	} else {
		return client.Err()
	}
}

// GetString 这边会有个redis.ErrNil,是string里抛出的，不是redis问题
func (rc *RClient) GetString(key string) (string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("GET", key)
		val, err := redis.String(v, err)
		return val, err
	} else {
		return "", client.Err()
	}
}

func (rc *RClient) MGetString(keys []string) ([]string, error) {
	if len(keys) == 0 {
		return []string{}, nil
	}
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		var args []interface{}
		for _, v := range keys {
			args = append(args, v)
		}
		v, err := client.Do("MGET", args...)

		val, _ := redis.Strings(v, err)
		return val, err
	} else {
		return []string{}, client.Err()
	}
}

func (rc *RClient) Set(params ...interface{}) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("SET", params...)

		if v == "OK" && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) SetEx(key string, value interface{}, expire int64) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("SETEX", key, expire, value)
		if v == "OK" && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) SetNx(key string, value interface{}) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("SETNX", key, value)
		if v == 1 && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) SetString(key, data string, exp int) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("SET", key, data, "EX", exp)
		if v == "OK" && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) SetInt(key string, data int, exp int) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("SET", key, data, "EX", exp)
		if v == "OK" && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) Hincrby(key, key2 string, step, expire int) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("HINCRBY", key, key2, step)
		val, _ := redis.Int(v, err)
		if val <= 1 {
			client.Do("EXPIRE", key, expire)
		}
		return val, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) Hmset(key string, dataMap map[string]string, expire int) (bool, error) {
	if len(dataMap) == 0 {
		return false, nil
	}
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := client.Do("HMSET", redis.Args{}.Add(key).AddFlat(dataMap))
		if v == "OK" && err == nil {
			client.Do("EXPIRE", key, expire)
			return true, err
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

func (rc *RClient) Hmget(key string, values []string) (map[string]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		args := redis.Args{}.Add(key).AddFlat(values)
		res, err := redis.Strings(client.Do("HMGET", args...))

		stringMap1 := map[string]string{}
		for i, hashv := range res {
			if hashv != "" {
				valKey := values[i]
				stringMap1[valKey] = hashv
			}
		}

		return stringMap1, err
	} else {
		return map[string]string{}, client.Err()
	}
}

func (rc *RClient) HGetString(key, value string) (string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		args := redis.Args{}.Add(key).Add(value)
		v, err := client.Do("HGET", args...)
		val, err := redis.String(v, err)
		return val, err
	} else {
		return "", client.Err()
	}
}

func (rc *RClient) HSet(key, field, value string) (bool, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		args := redis.Args{}.Add(key).Add(field).Add(value)
		v, err := client.Do("HSET", args...)
		if v == "OK" && err == nil {
			return true, nil
		}
		return false, err
	} else {
		return false, client.Err()
	}
}

// HDel 删除对应key的哈希中的field
func (rc *RClient) HDel(key, field string) error {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		_, err := client.Do("HDEL", key, field)
		if err == nil {
			return nil
		}
		return err
	} else {
		return client.Err()
	}

}

func (rc *RClient) Keys(keyPattern string) ([]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		args := redis.Args{}.Add(keyPattern)
		res, err := redis.Strings(client.Do("KEYS", args...))
		return res, err
	} else {
		return nil, client.Err()
	}
}

func (rc *RClient) Hmgetall(key string) (map[string]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.StringMap(client.Do("HMGETALL", redis.Args{}.Add(key)...))

		return v, err
	} else {
		return map[string]string{}, client.Err()
	}
}
func (rc *RClient) Hgetall(key string) (map[string]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.StringMap(client.Do("HGETALL", key))

		return v, err
	} else {
		return map[string]string{}, client.Err()
	}
}
func (rc *RClient) LPush(key string, values interface{}) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("LPUSH", redis.Args{}.Add(key).AddFlat(values)...))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) RPopInt(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("RPOP", key))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) RPopBytes(key string) ([]byte, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Bytes(client.Do("RPOP", key))
		return v, err
	} else {
		return []byte{}, client.Err()
	}
}

func (rc *RClient) LLen(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("LLEN", key))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) LRem(key string, count int, value string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("LRem", key, count, value))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) SIsMember(key string, value interface{}) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()

		v, err := redis.Int(client.Do("SISMEMBER", key, value))

		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) SMembers(key string) ([]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Strings(client.Do("SMEMBERS", key))
		return v, err
	} else {
		return []string{}, client.Err()
	}
}

func (rc *RClient) SRem(key string, members ...interface{}) (int, error) {
	args := make([]interface{}, 0)
	args = append(args, key)
	args = append(args, members...)

	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("SREM", args...))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) Exists(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("EXISTS", key))

		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) Expire(key string, ttl int) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("EXPIRE", key, ttl))

		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) TTL(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("TTL", key))

		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) SAdd(key string, values ...interface{}) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("SADD", redis.Args{}.Add(key).Add(values...)...))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) SCard(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("SCARD", key))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) ZAdd(key string, values ...interface{}) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("ZADD", redis.Args{}.Add(key).Add(values...)...))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) ZCard(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("ZCARD", key))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) ZREVRANGE(key string, start, stop int) ([]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Strings(client.Do("ZREVRANGE", key, start, stop))
		return v, err
	} else {
		return []string{}, client.Err()
	}
}

func (rc *RClient) Del(key string) (int, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int(client.Do("DEL", key))
		return v, err
	} else {
		return 0, client.Err()
	}
}

func (rc *RClient) BRpop(key string) ([]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Strings(client.Do("brpop", key, 10))
		return v, err
	} else {
		return nil, client.Err()
	}
}

// ZScore 返回有序集中，成员的分数值
func (rc *RClient) ZScore(key string, member string) (float64, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Float64(client.Do("ZSCORE", key, member))
		return v, err
	} else {
		return 0, client.Err()
	}
}

// ZRem 移除有序集合中的一个或多个成员
func (rc *RClient) ZRem(key string, members ...interface{}) (int64, error) {
	args := make([]interface{}, 0)
	args = append(args, key)
	args = append(args, members...)

	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Int64(client.Do("ZREM", args...))
		return v, err
	} else {
		return 0, client.Err()
	}
}

// ZRangeByScore 返回有序集合指定分数区间内的成员，分数从低到高
func (rc *RClient) ZRangeByScore(key string, min, max int64) ([]string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.Strings(client.Do("ZRANGEBYSCORE", key, min, max))
		return v, err
	} else {
		return nil, client.Err()
	}
}

// Srandmember 随机获取
func (rc *RClient) Srandmember(key string) (string, error) {
	client := rc.clientPool.Get()
	if client.Err() == nil {
		defer client.Close()
		v, err := redis.String(client.Do("Srandmember", key))
		return v, err
	} else {
		return "", client.Err()
	}
}

var updateRedisLock sync.RWMutex
var redisServerList = make(map[string]*redis.Pool)

type Options struct {
	MaxIdle     int
	MaxActive   int
	IdleTimeout int
	Host        string
	PassWd      string
	DbName      string
	DB          int
}

func Redis(args ...string) *RClient {
	dbName := "default"
	if len(args) > 0 {
		dbName = args[0]
	}
	var rc = &RClient{}
	if pool, ok := redisServerList[dbName]; ok {
		rc.SetPool(dbName, pool)
	} else {
		return nil
	}
	return rc
}

func InitRedis(dbName string, opt Options) {
	if opt.MaxIdle == 0 {
		opt.MaxIdle = 5
	}
	if opt.MaxActive == 0 {
		opt.MaxActive = 5
	}
	if opt.IdleTimeout == 0 {
		opt.MaxActive = 60
	}
	updateRedisLock.Lock()
	redisServerList[dbName] = func(opt Options) *redis.Pool {
		return &redis.Pool{
			MaxIdle:     opt.MaxIdle,
			MaxActive:   opt.MaxActive,
			Wait:        true,
			IdleTimeout: time.Duration(opt.IdleTimeout) * time.Second,
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", opt.Host,
					redis.DialPassword(opt.PassWd),
					redis.DialDatabase(opt.DB))
				if err != nil {
					return nil, err
				}
				return c, nil
			},
		}
	}(opt)
	updateRedisLock.Unlock()
}

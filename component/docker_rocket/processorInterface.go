package docker_rocket

import (
	"github.com/apache/rocketmq-client-go/v2/primitive"
)

type ProcessorInterface interface {
	Init() error
	QueueName() string
	Consumer(msg *primitive.MessageExt) error
	SetQueueName(name string)
}

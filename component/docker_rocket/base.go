package docker_rocket

import (
	"fmt"
	"go-beego-api/component/redis"
)

func getKey(queueName string) string {
	return fmt.Sprintf("statis:%s:remain", queueName)
}

func IncrRemain(queueName string) (int, error) {
	return redis.Redis("default").Incr(getKey(queueName), 86400*3)
}

func IncrRemainBy(queueName string, add int) (int, error) {
	return redis.Redis("default").IncrBy(getKey(queueName), add, 86400*3)
}

func DecrRemain(queueName string) (int, error) {
	return redis.Redis("default").Decr(getKey(queueName))
}

func GetRemain(queueName string) int {
	remain, _ := redis.Redis("default").GetInt(getKey(queueName))
	return remain
}

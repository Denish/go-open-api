package rabbitmq

import (
	"fmt"
	"github.com/streadway/amqp"
	"sync"
)

//ChannelPool amqp 通道池，提供通道复用
type ChannelPool struct {
	url      string
	mutex    sync.Mutex
	conn     *amqp.Connection
	channels []*amqp.Channel
}

//InitPool 初始化通道池
func (cp *ChannelPool) InitPool(url string, count ...int) {
	defCount := 1000
	if len(count) > 0 {
		defCount = count[0]
	}
	cp.url = url
	cp.channels = make([]*amqp.Channel, 0, defCount)
}

func (cp *ChannelPool) getChannel() (*amqp.Channel, error) {
	cp.mutex.Lock()
	defer cp.mutex.Unlock()
	size := len(cp.channels)
	if size > 0 {
		ch := cp.channels[size-1]
		cp.channels[size-1] = nil
		cp.channels = cp.channels[:size-1]
		return ch, nil
	}
	if cp.conn == nil {
		conn, err := amqp.Dial(cp.url)
		if err != nil {
			return nil, err
		}
		cp.conn = conn
	}
	ch, err := cp.conn.Channel()
	if err != nil {
		cp.conn.Close()
		cp.conn = nil
		return nil, err
	}
	return ch, nil
}

func (cp *ChannelPool) returnChannel(ch *amqp.Channel) {
	cp.mutex.Lock()
	cp.channels = append(cp.channels, ch)
	cp.mutex.Unlock()
}

func (cp *ChannelPool) checkConnect() {
	cp.mutex.Lock()
	defer cp.mutex.Unlock()
	if cp.conn == nil {
		return
	}
	ch, err := cp.conn.Channel()
	if err != nil {
		for i := 0; i < len(cp.channels); i++ {
			cp.channels[i].Close()
			cp.channels[i] = nil
		}
		cp.channels = cp.channels[0:0]
		cp.conn.Close()
		cp.conn = nil
		return
	}
	cp.channels = append(cp.channels, ch)
}

//Publish 发布消息
func (cp *ChannelPool) Publish(exchange, key string, mandatory, immediate, reliable bool, msg amqp.Publishing) error {
	ch, err := cp.getChannel()
	if err != nil {
		return err
	}
	if reliable {
		if err := ch.Confirm(false); err != nil {
			// base.Log.Error("Channel could not be put into confirm mode: %s", err)
		}

		confirms := ch.NotifyPublish(make(chan amqp.Confirmation, 1))

		err = ch.Publish(exchange, key, mandatory, immediate, msg)
		if err != nil {
			ch.Close()
			cp.checkConnect()
			return err
		}
		defer cp.ConfirmOne(confirms)

		return nil
	} else {
		err = ch.Publish(exchange, key, mandatory, immediate, msg)
		if err != nil {
			ch.Close()
			cp.checkConnect()
			return err
		}
	}

	cp.returnChannel(ch)
	return nil
}

func (cp *ChannelPool) ConfirmOne(confirms <-chan amqp.Confirmation) error {
	// base.Log.Info("waiting for confirmation of one publishing")

	if confirmed := <-confirms; confirmed.Ack {
		// base.Log.Info("confirmed delivery with delivery tag:", confirmed.DeliveryTag)
		return nil
	} else {
		// base.Log.Error("failed delivery of delivery tag:", confirmed.DeliveryTag)
		return fmt.Errorf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

//QueueDeclare 创建消息队列消息
func (cp *ChannelPool) QueueDeclare(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) error {
	ch, err := cp.getChannel()
	if err != nil {
		return err
	}
	_, err = ch.QueueDeclare(name, durable, autoDelete, exclusive, noWait, args)
	if err != nil {
		ch.Close()
		cp.checkConnect()
		return err
	}

	cp.returnChannel(ch)
	return nil
}

//QueueDeclare 确认消息队列消息是否存在
func (cp *ChannelPool) QueueDeclarePassive(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) error {
	ch, err := cp.getChannel()
	if err != nil {
		return err
	}
	_, err = ch.QueueDeclarePassive(name, durable, autoDelete, exclusive, noWait, args)
	if err != nil {
		ch.Close()
		cp.checkConnect()
		return err
	}

	cp.returnChannel(ch)
	return nil
}

//ExchangeDeclare 创建交换器
func (cp *ChannelPool) ExchangeDeclare(name, kind string, durable, autoDelete, internal, noWait bool, args amqp.Table) error {
	ch, err := cp.getChannel()
	if err != nil {
		return err
	}
	err = ch.ExchangeDeclare(name, kind, durable, autoDelete, internal, noWait, args)
	if err != nil {
		ch.Close()
		cp.checkConnect()
		return err
	}

	cp.returnChannel(ch)
	return nil
}

//ExchangeDeclare 确认交换器是否存在
func (cp *ChannelPool) ExchangeDeclarePassive(name, kind string, durable, autoDelete, internal, noWait bool, args amqp.Table) error {
	ch, err := cp.getChannel()
	if err != nil {
		return err
	}
	err = ch.ExchangeDeclarePassive(name, kind, durable, autoDelete, internal, noWait, args)
	if err != nil {
		ch.Close()
		cp.checkConnect()
		return err
	}

	cp.returnChannel(ch)
	return nil
}

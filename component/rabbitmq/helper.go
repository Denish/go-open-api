package rabbitmq

import (
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
)

// Session 获取一个池会话
func Session(names ...string) *Pool {
	name := "default"
	if len(names) > 0 {
		name = names[0]
	}
	if p, ok := pool[name]; ok {
		return p
	}
	return nil
}

// NewConnection 返回一个新的amqp链接
func NewConnection(names ...string) (conn *amqp.Connection, err error) {
	name := "default"
	if len(names) > 0 && names[0] != "" {
		name = names[0]
	}
	mqURL := viper.GetString("rabbitmq." + name + ".host")
	conn, err = amqp.Dial(mqURL)
	return
}

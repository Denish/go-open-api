package example

import (
	"io/ioutil"
	"net/textproto"
	m "niu-ren/component/mail"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-message/mail"
)

// Clear auth code !!!!!!

func TestQQMailClient(t *testing.T) {
	_, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	time.Sleep(60 * time.Second) // waitting and checking
}

func Test163MailClient(t *testing.T) {
	_, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	time.Sleep(60 * time.Second) // waitting and checking
}

func TestQQMailServerCapability(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	c, err := mc.ServerCapcity()
	if err != nil {
		t.Fatalf("fetching server capability fail:%v\n", err)
	}
	t.Logf("server capability is %v\n", c)
	time.Sleep(5 * time.Second) // waitting and checking
}

func Test163MailServerCapability(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	c, err := mc.ServerCapcity()
	if err != nil {
		t.Fatalf("fetching server capability fail:%v\n", err)
	}
	t.Logf("server capability is %v\n", c)
	time.Sleep(5 * time.Second) // waitting and checking
}

func Test163MailBoxlist(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	_, err = mc.MailBoxLists("", "*")
	if err != nil {
		t.Fatalf(" fetching server box list fail:%v\n", err)
	}
}

func TestQQMailBoxlist(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	_, err = mc.MailBoxLists("", "*")
	if err != nil {
		t.Fatalf(" fetching server box list fail:%v\n", err)
	}
}

func TestQQFetchEnvelope(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	seqSet := new(imap.SeqSet)
	seqSet.AddRange(mc.GetMailboxStatus().Messages, mc.GetMailboxStatus().Messages)
	msgs, err := mc.FetchMsgManually(seqSet, []imap.FetchItem{imap.FetchEnvelope})
	if err != nil {
		t.Fatalf("fetching envelop fail:%v\n", err)
	}
	for _, msg := range msgs {
		t.Logf("envelope of one msg is %#v\n", *msg.Envelope)
	}
}

func Test163FetchEnvelope(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	seqSet := new(imap.SeqSet)
	seqSet.AddRange(mc.GetMailboxStatus().Messages, mc.GetMailboxStatus().Messages)
	msgs, err := mc.FetchMsgManually(seqSet, []imap.FetchItem{imap.FetchEnvelope})
	if err != nil {
		t.Fatalf("fetching envelop fail:%v\n", err)
	}
	for _, msg := range msgs {
		t.Logf("envelope of one msg is %#v\n", *msg.Envelope)
	}
}

// TestQQSearch function of method isn't supported by server,seemingly !!!
func Test163Search(t *testing.T) {
	mc, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	criteria := imap.NewSearchCriteria()
	criteria.Since = time.Now().AddDate(0, 0, -2)
	criteria.SentSince = time.Now().AddDate(0, 0, -2)
	criteria.Or = [][2]*imap.SearchCriteria{{
		1: &imap.SearchCriteria{Header: textproto.MIMEHeader{"To": {"TensorSpeech/TensorFlowTTS"}}},
		0: &imap.SearchCriteria{Header: textproto.MIMEHeader{"Cc": {"Subscribed"}}},
	}}
	criteria.Header.Add("FROM", "2388297@163.com")
	criteria.Header.Add("Content-Type", "text/csv")
	criteria.Header.Add("Subject", "Re: [TensorSpeech/TensorFlowTTS] Fastspeech 2 Training error (Issue #778)")
	criteria.Not = []*imap.SearchCriteria{{Header: textproto.MIMEHeader{"From": {"hy绽"}}}}
	criteria.Smaller = (10 << 20) // 20M. 1 << 10 1Kb 1 << 20 1Mb 1 << 30 1Gb
	criteria.Body = []string{"view it on GitHub"}
	criteria.Text = []string{"view it on GitHub"}
	criteria.WithFlags = []string{imap.RecentFlag}
	criteria.WithoutFlags = []string{imap.DeletedFlag, imap.DraftFlag}
	ids, err := mc.SearchMsg(criteria)
	if err != nil {
		t.Fatalf("searching msg exception:%v\n", err)
	}
	t.Logf("search result ids are %v\n", ids)
}

// TestQQSearch function of method isn't supported by server,seemingly !!!
func TestQQSearch(t *testing.T) {
	mc, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.EnableMailboxWatch(false),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.EnableMailboxStatusUpdate(false),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	criteria := imap.NewSearchCriteria()
	criteria.Since = time.Now().AddDate(0, 0, -2)
	criteria.SentSince = time.Now().AddDate(0, 0, -2)
	h1 := textproto.MIMEHeader{}
	h2 := textproto.MIMEHeader{}
	h1.Add("To", strconv.QuoteToASCII("HY'绽"))
	h2.Add("Cc", "hy")
	c1 := &imap.SearchCriteria{Header: h1}
	c2 := &imap.SearchCriteria{Header: h2}
	criteria.Or = [][2]*imap.SearchCriteria{{c1, c2}}
	criteria.Header.Add("From", strconv.QuoteToASCII("QQ邮箱团队"))
	criteria.Header.Add("Content-Type", "text/csv")
	criteria.Header.Add("Subject", strconv.QuoteToASCII("更安全、更高效、更强大，尽在QQ邮箱APP")) //imap.RawString("APP 好"))
	criteria.Not = []*imap.SearchCriteria{{Header: textproto.MIMEHeader{"From": {"hy绽"}}}}
	criteria.Smaller = (10 << 20) // 20M. 1 << 10 1Kb 1 << 20 1Mb 1 << 30 1Gb
	criteria.Body = []string{"Training"}
	criteria.Text = []string{"Re: [TensorSpeech/TensorFlowTTS]"}
	criteria.WithoutFlags = []string{imap.DeletedFlag, imap.DraftFlag}
	ids, err := mc.SearchMsg(criteria)
	t.Logf("search result ids are %v\n", ids)
	if err != nil {
		t.Fatalf("searching msg exception:%v\n", err)
	}
	t.Logf("search result ids are %v\n", ids)
}

func Test163FetchMail(t *testing.T) {
	mc, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(30)
	msgs, err := mc.FetchMsgManually(seqSet, []imap.FetchItem{
		imap.FetchFlags, imap.FetchInternalDate, imap.FetchRFC822Size, imap.FetchEnvelope,
		new(imap.BodySectionName).FetchItem()})
	if err != nil {
		t.Fatalf("fetching msg by seqSet exception:%v\n", err)
	}
	parsed, err := mc.ParseMsgBody(msgs)
	if err != nil {
		t.Fatalf("parsing msg exception:%v\n", err)
	}
	for _, p := range parsed {
		for _, part := range p.BodyPart {
			switch part.PartType {
			case m.INLINE:
				_ = part.PartHeader.(*mail.InlineHeader)
			case m.ATTCHMENT:
				c, _ := ioutil.ReadAll(part.PartBody)
				fileName, _ := part.PartHeader.(*mail.AttachmentHeader).Filename()
				f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0766)
				if err == nil {
					f.Write(c)
					f.Close()
				}
			}
		}
	}
}

func TestQQFetchMail(t *testing.T) {
	mc, err := m.NewMailClient(
		m.EnableDebug(false),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	defer time.Sleep(5 * time.Second) // waitting and checking
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(mc.GetCurMailbox().Messages)
	msgs, err := mc.FetchMsgManually(seqSet, []imap.FetchItem{
		imap.FetchFlags, imap.FetchInternalDate, imap.FetchRFC822Size, imap.FetchEnvelope,
		new(imap.BodySectionName).FetchItem()})
	if err != nil {
		t.Fatalf("fetching msg by seqSet exception:%v\n", err)
	}
	parsed, err := mc.ParseMsgBody(msgs)
	if err != nil {
		t.Fatalf("parsing msg exception:%v\n", err)
	}
	for _, p := range parsed {
		for _, part := range p.BodyPart {
			switch part.PartType {
			case m.INLINE:
				_ = part.PartHeader.(*mail.InlineHeader)
			case m.ATTCHMENT:
				c, _ := ioutil.ReadAll(part.PartBody)
				fileName, _ := part.PartHeader.(*mail.AttachmentHeader).Filename()
				f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0766)
				if err == nil {
					f.Write(c)
					f.Close()
				}
			}
		}
	}
}

func TestQQMailServerIDLE(t *testing.T) {
	_, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(63),
		m.Account("913115877@qq.com"),
		m.AuthCode("ssvxuwdwpcqmbeba"),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	time.Sleep(60 * time.Minute) // waitting and checking
}

func Test163MailServerIDLE(t *testing.T) {
	_, err := m.NewMailClient(
		// m.EnableDebug(),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(63),
		m.Account("2388297@163.com"),
		m.AuthCode("NYIFXVUXMGOWKHFS"),
		m.WatchMailboxPollingInterval(30),
		m.ServerAddr(true, "imap.163.com:993"),
	)
	if err != nil {
		t.Fatalf("Creating mail client fail:%v\n", err)
	}
	t.Log("creating mail client done")
	time.Sleep(60 * time.Minute) // waitting and checking
}

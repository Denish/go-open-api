package mail

type MailType int

const (
	QQMAIL MailType = iota + 1
	WY163MAIL
	WY126MAIL
)

type ConnState int

const (
	CONNECTED ConnState = iota + 1
	DISCONNECT
	SHUTDOWN
)

const DEFAULTMAILBOX = "INBOX"

type Extension string

const (
	IDLE        Extension = "IDLE"
	MOVE        Extension = "MOVE"
	ENABLE      Extension = "ENABLE"
	CHILDREN    Extension = "CHILDREN"
	LITERALPLUS Extension = "LITERAL+"
	SASLIR      Extension = "SASL-IR"
	UNSELECT    Extension = "UNSELECT"
	IMPORTANT   Extension = "IMPORTANT"
	SPECIALUSE  Extension = "SPECIAL-USE"
	APPENDLIMIT Extension = "APPENDLIMIT"
)

type BodyPart int

const (
	INLINE BodyPart = iota + 1
	ATTCHMENT
)

const SEPERATION string = "------------------"

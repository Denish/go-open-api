package example

import (
	"fmt"
	m "niu-ren/component/mail"
	"os"
)

func ExampleQQMailClient() {
	_, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.qq.com:993"),
		m.Account(""),  // replacing yourself mail account
		m.AuthCode(""), // replacing yourself mail safe code
	)
	if err != nil {
		panic(fmt.Sprintf("Creating mail client fail:%v\n", err))
	}
}

func Example163MailClient() {
	_, err := m.NewMailClient(
		m.EnableDebug(true),
		m.DebugOutput(os.Stdout),
		m.WatchMailboxTimeout(600),
		m.WatchMailboxPollingInterval(60),
		m.ServerAddr(true, "imap.163.com:993"),
		m.Account(""),  // replacing yourself mail account
		m.AuthCode(""), // replacing yourself mail safe code
	)
	if err != nil {
		panic(fmt.Sprintf("Creating mail client fail:%v\n", err))
	}
}

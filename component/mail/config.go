package mail

import (
	"io"
	"time"
)

type Config struct {
	account,
	authCode,
	curMailbox,
	serverAddr,
	password string
	debugMode,
	sslEncrypt,
	enableMailboxStatusUpdate,
	enableMailboxWatch, isOn bool
	debugOut                                                                      io.Writer
	watchMailboxTimeout, pollingInterval, updateMailboxStatusInterval, cmdTimeout time.Duration
}

type Option func(*MailClient)

// CurMailbox: default INBOX
func CurMailbox(mailBox string) Option {
	return func(mc *MailClient) {
		mc.config.curMailbox = mailBox
	}
}

func Account(account string) Option {
	return func(mc *MailClient) {
		mc.config.account = account
	}
}

// Password: authCode take precedence over password.
func Password(pwd string) Option {
	return func(mc *MailClient) {
		mc.config.password = pwd
	}
}

// AuthCode: taking precedence over password.
func AuthCode(authCode string) Option {
	return func(mc *MailClient) {
		mc.config.authCode = authCode
	}
}

func ServerAddr(sslEncrypt bool, addr string) Option {
	return func(mc *MailClient) {
		mc.config.serverAddr = addr
		mc.config.sslEncrypt = sslEncrypt
	}
}

// EnableMailboxWatch: mailbox watching is enabled,by default.
func EnableMailboxWatch(isEnable bool) Option {
	return func(mc *MailClient) {
		mc.config.enableMailboxWatch = isEnable
	}
}

// WatchMailboxTimeout: to avoid unexpected being logged out by server.restarting watch after seconds
func WatchMailboxTimeout(second int) Option {
	return func(mc *MailClient) {
		mc.config.watchMailboxTimeout = time.Second * time.Duration(second)
	}
}

// WatchMailboxPollingInterval: if server doesn't support IDLE protocol,Idle() method of go-imap package will fall back to
// polling way with interval time, one minute by default.
func WatchMailboxPollingInterval(second int) Option {
	return func(mc *MailClient) {
		mc.config.pollingInterval = time.Second * time.Duration(second)
	}
}

func EnableDebug(enable bool) Option {
	return func(mc *MailClient) {
		mc.config.debugMode = enable
	}
}

func DebugOutput(w io.Writer) Option {
	return func(mc *MailClient) {
		mc.config.debugOut = w
	}
}

func SetMailboxMsgCb(cb IMailboxMsgCb) Option {
	return func(mc *MailClient) {
		mc.IMailboxMsgCb = cb
	}
}

func UpdateMailboxStatusInterval(t int) Option {
	return func(mc *MailClient) {
		mc.config.updateMailboxStatusInterval = time.Duration(t) * time.Second
	}
}

func CmdTimeout(t int) Option {
	return func(mc *MailClient) {
		mc.config.cmdTimeout = time.Duration(t) * time.Second
	}
}

func EnableMailboxStatusUpdate(enable bool) Option {
	return func(mc *MailClient) {
		mc.config.enableMailboxStatusUpdate = enable
	}
}

func IsOn(isOn bool) Option {
	return func(mc *MailClient) {
		mc.config.isOn = isOn
	}
}

package dingtalk

import (
	"testing"
)

var ROBOT_TOKEN = "e60d2f0f72091a033a51a6b322c37626b58b0b8bca176f9504de2da399e3d248"
var ROBOT_SECRET = "SEC5f5faa18977a7c30a9883489d29f61a023c2684d2e30a03733d5112f78321548"

func TestRobot_SendMessage(t *testing.T) {
	//t.SkipNow()

	msg := map[string]interface{}{
		"msgtype": "text",
		"text": map[string]string{
			"content": "这是一条golang钉钉消息测试.",
		},
		"at": map[string]interface{}{
			"atMobiles": []string{},
			"isAtAll":   false,
		},
	}

	robot := NewRobot(ROBOT_TOKEN, ROBOT_SECRET)
	if err := robot.SendMessage(msg); err != nil {
		t.Error(err)
	}
}

func TestRobot_SendTextMessage(t *testing.T) {
	robot := NewRobot(ROBOT_TOKEN, ROBOT_SECRET)
	if err := robot.SendTextMessage("普通文本消息", []string{}, false); err != nil {
		t.Error(err)
	}
}

func TestRobot_SendMarkdownMessage(t *testing.T) {
	robot := NewRobot(ROBOT_TOKEN, ROBOT_SECRET)
	err := robot.SendMarkdownMessage(
		"Markdown Test Title",
		"### Markdown 测试消息\n* 谷歌: [Google](https://www.google.com/)\n* 一张图片\n ![](https://avatars0.githubusercontent.com/u/40748346)",
		[]string{},
		false,
	)
	if err != nil {
		t.Error(err)
	}
}

func TestRobot_SendLinkMessage(t *testing.T) {
	robot := NewRobot(ROBOT_TOKEN, ROBOT_SECRET)
	err := robot.SendLinkMessage(
		"Link Test Title",
		"这是一条链接测试消息",
		"https://github.com/JetBlink",
		"https://avatars0.githubusercontent.com/u/40748346",
	)

	if err != nil {
		t.Error(err)
	}
}

package hwcobs

import (
	"fmt"
	"testing"
)

func TestPutFile(t *testing.T) {
	InitObs("NKP1XP5AJMZPTWIXAZST", "S6wXwgKufXuJPTxZnbhEghFEGV2z7PNWY4HecLHR",
		"https://obs.cn-east-3.myhuaweicloud.com", "chanke-test", "https://ck-scrm-cdn-test.cds8.cn")
	fmt.Println(PutFile("/Users/chandashi/Downloads/107627-1620284428.jpg", "testupload/cccc.jpg"))
}

func TestCreateSignedUrl(t *testing.T) {
	InitObs("NKP1XP5AJMZPTWIXAZST", "S6wXwgKufXuJPTxZnbhEghFEGV2z7PNWY4HecLHR",
		"https://obs.cn-east-3.myhuaweicloud.com", "chanke-test", "https://ck-scrm-cdn-test.cds8.cn")

	fmt.Println(CreateSignedUrl("testupload/cccc.jpg"))
}

//xxx.com/chatfile/date/cid/md5.后缀

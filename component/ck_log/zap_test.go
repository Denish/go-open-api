package ck_log

import (
	"errors"
	"go.uber.org/zap"
	"testing"
	"time"
)

// https://blog.csdn.net/u012124304/article/details/111405222

func TestInitZap(t *testing.T) {
	InitZap(true)
	// 打印日志
	Logger.Debugf("i am debug, using %s", "sugar") // 这行不会打印，因为日志级别是 INFO
	Logger.Infof("i am info, using %s", "sugar")   // INFO  级别日志，这个会正常打印
	Logger.Warnf("i am warn, using %s", "sugar")   // WARN  级别日志，这个会正常打印
	Logger.Errorf("i am error, using %s", "sugar") // ERROR 级别日志，这个会打印，并附带堆栈信息
	// Logger.Fatalf("i am fatal, using %s", "sugar") // FATAL 级别日志，这个会打印，附带堆栈信息，并调用 os.Exit 退出

	aa := Logger.With("Class", "Mysql", "ExecTime", 121.11)
	aa = aa.With("zzz", []int{1, 3, 5})
	aa.Errorf("i am error2, using %s", `suga
rasdfa
asdf
asdf
`)

	Logger.Infow("i am info, using sugar", zap.String("aaa", "bbb"))

	Logger.Error(errors.New("aaaa"))
	for i := 0; i < 10; i++ {
		time.Sleep(10 * time.Millisecond)
		Logger.Infow("i am info, using sugar", zap.String("aaa", time.Now().String()))
	}
}

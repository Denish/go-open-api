package main

import (
	_ "go-beego-api/app/routers"
	"go-beego-api/bootstrap"
)

func main() {

	// 初始化组件库
	bootstrap.ApiInit()

	// 启动WebServer
	bootstrap.Run()
}

package logic

import (
	syscontext "context"
	modelDb "go-beego-api/app/models/db"
	"go-beego-api/services/openai"
)

// OpenAiLogic 回调通用结构体
type OpenAiLogic struct {
	ctx  syscontext.Context
	user *modelDb.User
}

// NewOpenAiLogic ...
func NewOpenAiLogic(ctx syscontext.Context, user *modelDb.User) *OpenAiLogic {
	return &OpenAiLogic{ctx: ctx, user: user}
}

func (l *OpenAiLogic) Openai(prompt string) (resp string, err error) {
	resp, err = openai.NewOpenAI().GetTextResult(prompt)
	return
}

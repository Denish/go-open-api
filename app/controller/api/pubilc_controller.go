package api

import (
	sysContext "context"
	"go-beego-api/app/controller"
	"go-beego-api/app/logic"
)

type PublicController struct {
	controller.BaseController
	ctx sysContext.Context
}

//Prepare 预处理
func (c *PublicController) Prepare() {
	c.BaseController.Prepare()
	c.ctx = c.SysContextWithClass("[PublicCtl]")
}

func (c *PublicController) OpenAi() {
	prompt := c.GetString("prompt", "")
	resp, err := logic.NewOpenAiLogic(c.ctx, nil).Openai(prompt)
	if err != nil {
		c.TraceErrorResp(err)
		return
	}
	c.TraceSuccessResp(resp)
}

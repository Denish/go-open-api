package controller

import (
	"strconv"
)

// InputMap 数据输入类型
type InputMap map[string]interface{}

// get 从map中获取数据
func (c InputMap) get(key string) interface{} {
	if c == nil || key == "" {
		return nil
	}
	if _, ok := c[key]; ok {
		return c[key]
	}
	return nil
}

// GetInt 从map中获取int类型数据
func (c InputMap) GetInt(key string, defaultVal int) int {
	origin := c.get(key)
	if _, ok := origin.(float64); ok {
		return int(origin.(float64))
	} else if val, ok := origin.(string); ok {
		i, err := strconv.Atoi(val)
		if err != nil {
			return defaultVal
		}
		return i
	}
	return defaultVal
}

// Get 从map中获取interface类型数据
func (c InputMap) Get(key string) interface{} {
	return c.get(key)
}

// GetArrArrString 获取二维数组
func (c InputMap) GetArrArrString(key string) [][]string {
	origin := c.GetArr(key)
	var ret [][]string
	for _, v := range origin {
		var subRet []string
		if stt, ok := v.([]interface{}); ok {
			for _, v2 := range stt {
				if str, ok := v2.(string); ok {
					subRet = append(subRet, str)
				}
			}
			ret = append(ret, subRet)
		}
	}
	return ret
}

// GetIntBool 获取int类型的bool数据 true=1 false=0
func (c InputMap) GetIntBool(key string, defaultVal int) int {
	origin := c.get(key)
	if _, ok := origin.(float64); ok {
		boolData := int(origin.(float64))
		if boolData != 0 && boolData != 1 {
			boolData = defaultVal
		}
		return boolData
	}
	return defaultVal
}

// GetInt64 获取int64的数据
func (c InputMap) GetInt64(key string, defaultVal int64) int64 {
	origin := c.get(key)
	if _, ok := origin.(float64); ok {
		return int64(origin.(float64))
	}
	return defaultVal
}

// GetFloat64 获取float64数据
func (c InputMap) GetFloat64(key string, defaultVal float64) float64 {
	origin := c.get(key)
	if _, ok := origin.(float64); ok {
		return origin.(float64)
	}
	return defaultVal
}

// GetString 获取字符串类型数据
func (c InputMap) GetString(key string, defaultVal string) string {
	origin := c.get(key)
	if origin != nil {
		if kd, ok := origin.(string); ok {
			if kd == "" {
				return defaultVal
			}
			return kd
		}
	}
	return defaultVal
}

// GetStringWithEmpty 获取字符串数据，默认值为空字符串
func (c InputMap) GetStringWithEmpty(key string) string {
	return c.GetString(key, "")
}

// GetStringWithQualified 获取字符串数据，可以指定限定选项
func (c InputMap) GetStringWithQualified(key string, def string, qualified ...string) string {
	str := c.GetString(key, def)
	if str != def {
		allowed := false
		for _, v := range qualified {
			if v == str {
				allowed = true
			}
		}
		if allowed {
			return str
		}
		return def
	}
	return str
}

// GetPage 获取page数据
func (c InputMap) GetPage(key ...string) int {
	keyName := "page"
	if len(key) >= 1 {
		keyName = key[0]
	}
	val := c.GetInt(keyName, 1)
	return val
}

// GetPageSize 获取pageSize
func (c InputMap) GetPageSize(key string, defSize int, maxSize int) (size int) {
	//var err error
	size = defSize
	size = c.GetInt(key, defSize)
	if size > maxSize {
		size = maxSize
	}
	return
}

// GetBool 获取bool类型的值
func (c InputMap) GetBool(key string, defaultVal bool) bool {
	origin := c.get(key)
	if _, ok := origin.(bool); ok {
		return origin.(bool)
	}
	return defaultVal
}

// Exists 是否存在key
func (c InputMap) Exists(key string) bool {
	origin := c.get(key)
	return origin != nil
}

// GetArr 获取数组
func (c InputMap) GetArr(key string) []interface{} {
	origin := c.get(key)
	if _, ok := origin.([]interface{}); ok {
		return origin.([]interface{})
	}
	return nil
}

// GetArrString 获取字符串数组
func (c InputMap) GetArrString(key string) []string {
	origin := c.GetArr(key)
	if origin != nil {
		var ret []string
		for _, v := range origin {
			if _, ok := v.(string); ok {
				ret = append(ret, v.(string))
			}
		}
		return ret
	}
	return nil
}

// GetArrInt64 获取int64数组
func (c InputMap) GetArrInt64(key string) []int64 {
	origin := c.GetArr(key)
	if origin != nil {
		var ret []int64
		for _, v := range origin {
			if floatData, ok := v.(float64); ok {
				ret = append(ret, int64(floatData))
			}
		}
		return ret
	}
	return nil
}

// GetArrInt 获取int数组
func (c InputMap) GetArrInt(key string) []int {
	origin := c.GetArr(key)
	if origin != nil {
		var ret []int
		for _, v := range origin {
			if floatData, ok := v.(float64); ok {
				ret = append(ret, int(floatData))
			}
		}
		return ret
	}
	return nil
}

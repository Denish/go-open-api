package redis

import (
	"fmt"
	"time"
)

//存放redisKey

const (
	LoginTokenKey = "clock:fe:user:token:%s:%d" // userId:roleId
	MobileCodeKey = "clock:fe:mobileCode:%s:%s" //手机号验证码

	SmsCodeKey   = "clock:fe:sms:%s:%s"       // 短信防刷
	SmsCodeIpKey = "clock:fe:sms:ip:%s:%s:%d" // 短信ip防刷
	RepeatApiKey = "clock:fe:repeat:%s"       // 接口防刷

	UserInfoKey = "clock:user:info:%s" // 用户信息

	WxaCodeCache = "clock:wxa:code:%s" // 小程序code的cache, 有效期3小时

)

// Get 获取Key
func Get(key string, v ...interface{}) string {
	return fmt.Sprintf(key, v...)
}

func GetLoginTokenKey(userId string, roleID int) string {
	return fmt.Sprintf(LoginTokenKey, userId, roleID)
}

func GetSmsCodeKey(class, phone string) string {
	return fmt.Sprintf(SmsCodeKey, class, phone)
}

func GetSmsCodeIpKey(ip string) string {
	theDay := time.Now().Format("20060102")
	theHours := time.Now().Hour()
	return fmt.Sprintf(SmsCodeIpKey, ip, theDay, theHours)
}

func GetRepeatKey(str string) string {
	return fmt.Sprintf(RepeatApiKey, str)
}

func GetWxaCodeKey(str string) string {
	return fmt.Sprintf(WxaCodeCache, str)
}

package redis

type WxaCodeInfo struct {
	Code       string `json:"code"`
	OpenID     string `json:"open_id"`
	UnionID    string `json:"union_id"`
	SessionKey string `json:"session_key"`
}

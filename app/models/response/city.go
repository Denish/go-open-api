package response

type UserCityInputReq struct {
	CityID   int    `json:"city_id"`
	CityName string `json:"city_name"`
	PCity    *UserCityInputReq
}

type CityInputListReq struct {
	Id   int                 `json:"id"`
	Name string              `json:"name"`
	City []*CityInputListReq `json:"city"`
}

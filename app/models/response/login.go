package response

type LoginRspUserItem struct {
	AvatarURL     string            `json:"avatarUrl"`
	City          *UserCityInputReq `json:"city"`
	NickName      string            `json:"nick_name"`
	Province      string            `json:"province"`
	Gender        int               `json:"gender"`
	UserSid       string            `json:"user_sid"`        // uid
	RegTime       string            `json:"reg_time"`        // 注册时间
	LastLoginTime string            `json:"last_login_time"` // 最后一次登录时间
	LastIP        string            `json:"last_ip"`         // 最后登录的IP
	Token         string            `json:"token"`           // token字串
	ExpireTime    int64             `json:"expire_time"`     // 过期时间
	PatriarchCode string            `json:"patriarch_code"`
	UserType      int               `json:"user_type"`
}

type UserLoginRsp struct {
	LoginRspUserItem
}

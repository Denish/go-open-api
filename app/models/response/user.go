// Package response @Title 用户相关响应数据
package response

// Login1Res 登陆响应数据
type Login1Res struct {
	Token      string `json:"token"`       // token字串
	ExpireTime int64  `json:"expire_time"` // 过期时间
}

// UserInfoRes 用户信息响应结构
type UserInfoRes struct {
	AvatarURL     string `json:"avatar_url"`
	NickName      string `json:"nick_name"`
	Province      string `json:"province"`
	Gender        int    `json:"gender"`
	UserSid       string `json:"user_sid"` // uid
	PatriarchCode string `json:"patriarch_code"`
	UserType      int    `json:"user_type"`
}

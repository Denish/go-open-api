package response

const (
	//ClockTaskStatusIsClock 打卡状态：打卡
	ClockTaskStatusIsClock = 1
	//ClockTaskStatusNotClock 打卡状态：未打卡
	ClockTaskStatusNotClock = 2
)

type ClockTaskResp struct {
	TaskUUID    string `json:"task_uuid" `
	TaskName    string `json:"task_name" `
	TaskContent string `json:"task_content" `
	TaskScore   int    `json:"task_score" `
	StartTime   int64  `json:"start_time" `
	EndTime     int64  `json:"end_time" `
	Status      int    `json:"status"`
}

type ClockTaskItemResp struct {
	TaskUUID    string `json:"task_uuid" `
	TaskName    string `json:"task_name" `
	TaskContent string `json:"task_content" `
	TaskScore   int    `json:"task_score" `
	StartTime   int64  `json:"start_time" `
	EndTime     int64  `json:"end_time" `
}

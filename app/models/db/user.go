package db

import (
	sysContext "context"
	"errors"
	"go-beego-api/component/ck_log"
	"go.uber.org/zap"
	"time"
	"xorm.io/xorm"
)

type User struct {
	ID            int       `json:"id" xorm:"not null pk autoincr INT"`
	AvatarURL     string    `json:"avatar_url" xorm:"comment('头像URL') TEXT"`
	UserSid       string    `json:"user_sid" xorm:"comment('用户唯一UUID') VARCHAR(60)"`
	Phone         string    `json:"phone" xorm:"comment('手机') VARCHAR(30)"`
	CityID        int       `json:"city_id" xorm:"comment('城市id') index INT"`
	NickName      string    `json:"nick_name" xorm:"comment('昵称') VARCHAR(30)"`
	Province      string    `json:"province" xorm:"comment('省份') VARCHAR(30)"`
	Status        int       `json:"status" xorm:"comment('当前状态') TINYINT(1)"`
	RegTime       time.Time `json:"reg_time" xorm:"comment('注册时间') DATETIME"`
	LastLoginTime time.Time `json:"last_login_time" xorm:"comment('最后登录时间') DATETIME"`
	LastIP        string    `json:"last_ip" xorm:"comment('最后登录IP') VARCHAR(100)"`
	WechatNum     string    `json:"wechat_num" xorm:"not null default '' comment('微信号') VARCHAR(30)"`
	Gender        int       `json:"gender" xorm:"not null default 3 comment('性别：1男性 2女性 3未知') TINYINT(1)"`
	Birthday      time.Time `json:"birthday" xorm:"comment('生日') DATETIME"`
	UnionID       string    `json:"union_id" xorm:"comment('微信平台unionid') VARCHAR(40)"`
	WxaOpenID     string    `json:"wxa_open_id" xorm:"comment('小程序openid') VARCHAR(40)"`
	WxaSessionKey string    `json:"wxa_session_key" xorm:"VARCHAR(255)"`
	OaOpenID      string    `json:"oa_open_id" xorm:"comment('服务号openid') VARCHAR(40)"`
	OaStatus      int       `json:"oa_status" xorm:"comment('服务号关注状态； 1.已关注; 2.未关注') TINYINT(1)"`
	PatriarchCode string    `json:"patriarch_code" xorm:"comment('家长绑定号') VARCHAR(10)"`
	UserType      int       `json:"user_type" xorm:"not null default 3 comment('身份类型 1家长 2孩子') TINYINT(1)"`
}

func NewUser() *User {
	return new(User)
}

const (
	NrUserStatusIsOpen = 1 // 已启用
	NrUserStatusIsOff  = 2 // 已停用

	NrUserOaStatusSub   = 1 // 已订阅
	NrUserOaStatusUnSub = 2 // 未订阅
)

func (m *User) Add(ctx sysContext.Context, session *xorm.Session) (exists bool, err error) {
	if session == nil {
		session = GetMasterDbSession(ctx)
	}
	var row int64
	row, err = session.Insert(m)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("新增用户失败", zap.Error(err))
		err = errors.New("系统出错,新增用户失败")
	}
	if row > 0 {
		exists = true
	}
	return
}
func (m *User) GetWithID(ctx sysContext.Context, session *xorm.Session, id int) (exists bool, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	_, err = session.Where("id=?", id).Get(m)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据用户id查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户id查找用户信息失败")
	}
	if m.ID > 0 {
		exists = true
	}
	return
}

func (m *User) GetWithUUID(ctx sysContext.Context, session *xorm.Session, uuid string) (exists bool, user *User, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	user = new(User)
	_, err = session.Where("user_sid=?", uuid).Get(user)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据用户uuid查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户uuid查找用户信息失败")
	}
	if user.ID > 0 {
		exists = true
	}
	return
}

func (m *User) GetWithUnionID(ctx sysContext.Context, session *xorm.Session, unionid string) (exists bool, user *User, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	user = new(User)
	_, err = session.Where("union_id=?", unionid).Get(user)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据用户微信id查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户微信id查找用户信息失败")
	}
	if user.ID > 0 {
		exists = true
	}
	return
}
func (m *User) GetWithWxaOpenID(ctx sysContext.Context, session *xorm.Session, openid string) (exists bool, user *User, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	user = new(User)
	_, err = session.Where("wxa_open_id=?", openid).Get(user)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据用户小程序id查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户小程序id查找用户信息失败")
	}
	if user.ID > 0 {
		exists = true
	}
	return
}

func (m *User) Update(ctx sysContext.Context, session *xorm.Session, cols ...string) (row int64, err error) {
	if session == nil {
		session = GetMasterDbSession(ctx)
	}
	if m.ID == 0 {
		return 0, errors.New("缺少ID")
	}
	if len(cols) > 0 {
		session.Cols(cols...)
	}
	row, err = session.ID(m.ID).Update(m)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("修改用户失败", zap.Error(err))
		err = errors.New("系统出错,修改用户失败")
	}
	if row == 0 {
		err = errors.New("无数据更新")
		return
	}
	return
}

func (m *User) GetWithPhone(ctx sysContext.Context, session *xorm.Session, s string) (exists bool, user *User, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	user = new(User)
	_, err = session.Where("phone=?", s).Get(user)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据用户手机号查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户手机号查找用户信息失败")
	}
	if user.ID > 0 {
		exists = true
	}
	return
}

func (m *User) GetWithPhoneAndNormalStatus(ctx sysContext.Context, session *xorm.Session, s string) (exists bool, user *User, err error) {
	if session == nil {
		session = GetSlaveDbSession(ctx)
	}
	user = new(User)
	_, err = session.Where("phone=?", s).Where("status=1").Get(user)
	if err != nil {
		ck_log.LogCtx(ctx).Errorw("根据未被禁用用户手机号查找用户信息失败", zap.Error(err))
		err = errors.New("系统出错,根据用户手机号查找用户信息失败")
	}
	if user.ID > 0 {
		exists = true
	}
	return
}

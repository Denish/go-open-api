// Package request @Title 任务接口参数
package request

// WechatEventReq 事件请求
type WechatEventReq struct {
	Event string `json:"event" form:"event" validate:"required,oneof='report'" comment:"事件"`
}

type WechatLoginReq struct {
	Code string `json:"code"`
}

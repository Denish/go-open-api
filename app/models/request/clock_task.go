package request

type ClockTaskListReq struct {
	TaskType int `json:"task_type"`
}

type ClockTaskAddReq struct {
	TaskName    string `json:"task_name" validate:"required" message:"请填写任务名称"`
	TaskContent string `json:"task_content" validate:"required" message:"请填写任务明细"`
	TaskScore   int    `json:"task_score" validate:"required" message:"请填写任务积分"`
	StartTime   int64  `json:"start_time" validate:"required" message:"请填写任务时间"`
}

type ClockTaskUpdateReq struct {
	TaskSID string
	ClockTaskAddReq
}

type ClockTaskDelReq struct {
	TaskSID string
}

type ClockTaskFinishReq struct {
	TaskSID string
}

package request

type CorpLoginReq struct {
	CorpSID  string `json:"corp_sid" validate:"required" message:"缺少corp_sid参数"` // 企业id
	Phone    string `json:"phone" validate:"required" message:"请输入手机号"`          // 手机号
	Password string `json:"password"`                                            // 密码, 用于密码登录
	Code     string `json:"code"`                                                // 短信验证码, 用于验证码登录
}

var (
	SceneLogin     = "login"
	SceneRegister  = "register"
	SceneJoin      = "join"
	SceneInfo      = "info"
	SceneEditPwd   = "editPwd"   // 修改用户密码
	SceneEditPhone = "editPhone" // 修改用户手机号
)

type SendCodeReq struct {
	Phone         string `json:"phone" validate:"required" message:"请输入手机号"`
	CaptchaTicket string `json:"captcha_ticket" validate:"required" message:"请先通过人机验证"`
	Scene         string `json:"scene" validate:"required,oneof=login register join info"`
}

type UserLoginReq struct {
	Phone    string `json:"phone" validate:"required" message:"请输入手机号"` // 手机号
	Password string `json:"password"`                                   // 密码, 用于密码登录
	Code     string `json:"code"`                                       // 短信验证码, 用于验证码登录
}

type UserRegisterReq struct {
	Phone    string `json:"phone" validate:"required" message:"请输入手机号"`
	Name     string `json:"name" validate:"required" message:"请输入姓名"`
	Password string `json:"password" validate:"required" message:"请输入密码"`
	Code     string `json:"code" validate:"required" message:"请输入验证码"`
}

type IsUserRegisterReq struct {
	Phone string `json:"phone" validate:"required" message:"请输入手机号"`
}

// Package request @Title 用户相关请求数据
package request

const (
	//UserTypeIsPatriarch 用户身份类型 ：家长
	UserTypeIsPatriarch = 1
	//UserTypeIsChild 用户身份类型 ：孩子
	UserTypeIsChild = 2
)

// UpdateUserInfoReq 修改用户信息参数
type UpdateUserInfoReq struct {
	FileUrl    string `json:"file_url" `
	Nickname   string `json:"nickname" `    // 昵称
	GenderCode int    `json:"gender_code" ` // 性别编码
	CityCode   int    `json:"city_code" `
	Birthday   string `json:"birthday" "`                                      // 出生年月，格式yyyy-mm-dd
	UserType   int    `json:"user_type" validate:"required" message:"请选择身份类型"` //1家长 2孩子
}

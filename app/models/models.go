package models

import (
	"errors"
)

var (
	// ErrNotFound ...
	ErrNotFound = errors.New("data not found")
)

// TokenData token的信息
type TokenData struct {
	UUID       string `json:"id"`
	RoleID     int    `json:"role_id"`
	Token      string `json:"token"`
	ExpireTime int64  `json:"expire_time"`
	PlatformID int    `json:"platform_id"`
}

type AnalogInfo struct {
	CorpSid string `form:"corp_sid" json:"corp_sid"  `
	UserSid string `form:"user_sid" json:"user_sid"  `
}

// Token token的信息
type Token struct {
	Token      string `json:"token"`
	ExpireTime int64  `json:"expire_time"`
}

package routers

import (
	"github.com/astaxie/beego"
	"go-beego-api/app/controller/api"
)

func init() {
	// 公共接口
	//获取城市下拉

	beego.Router("/v1/api/public/openai", &api.PublicController{}, "get:OpenAi")

}

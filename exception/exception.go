// Package exception 定义API接口错误
package exception

import (
	"fmt"
	"runtime"
	"strings"
)

// Error 错误类型
type Error struct {
	Code    int
	Message string
}

// Error 返回错误信息
func (e *Error) Error() string {
	return e.Message
}

// ErrCode 返回错误码
func (e *Error) ErrCode() int {
	return e.Code
}

// NewError new a error
func NewError(code int, message string) *Error {
	return &Error{
		Code:    code,
		Message: message,
	}
}

// Message generate a general message error
func Message(message string) *Error {
	return &Error{
		Code:    SystemErrorCode,
		Message: message,
	}
}

type CanSeeError struct {
	Code    int
	Message string
}

//Error 返回错误信息
func (e *CanSeeError) Error() string {
	return e.Message
}

// NewCanSeeError ...
func NewCanSeeError(code int, message string) error {
	return &CanSeeError{
		Code:    code,
		Message: message,
	}
}

// GetStacks ...
func GetStacks() string {
	var stack []string
	for i := 1; ; i++ {
		_, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		stack = append(stack, fmt.Sprintf("%s:%d", file, line))
	}
	joinStr := ", "
	if RunMode == DEV { // 测试环境换行显示
		joinStr = "\r\n"
	}
	return strings.Join(stack, joinStr)
}

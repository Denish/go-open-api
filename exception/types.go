package exception

import uuid "github.com/satori/go.uuid"

var (
	RunMode = DEV     // dev/test/stage/prod
	Domain  = "NRSvc" // api/crontab/queue
)

func SetDomain(domain string) {
	Domain = domain + "_" + uuid.NewV4().String()
}

func SetDomainWithUUID(domain string) {
	Domain = domain
}

const (
	DEV   = "dev"
	TEST  = "test"
	STAGE = "stage"
	PROD  = "prod"
)

// 定义错误
const (
	RequestParamErrorCode        = 40000 // RequestParamErrorCode 参数错误
	AccountOrPasswordUnmatchCode = 40001 // 用户登录授权错误
	LoginInvalidCode             = 40002
	LoginExpiredCode             = 40003
	AccountNotExistCode          = 40004
	PhoneFormatErrorCode         = 40005 // 请输入正确手机号
	IPRequestSmsTooMuchCode      = 40006 // 当前IP请求验证码太频繁
	SliderVerifyCode             = 40007 // 滑块验证失败
	LoginCodeCode                = 40008 // 短信验证码错误
	PhoneNotRegCode              = 40009 // 该手机号未注册
	SendCodeErrorCode            = 40009 // 人机校验未通过
	AccountExistsCode            = 40010 // 账号已存在
	AccountNotExistsCode         = 40011 // 账号不存在
	SystemErrorCode              = 50000 // 系统通用报错
	ParametersInvalidCode        = 50001
	FileFormatIsWrongCode        = 50002
	FileTooLargeCode             = 50003
	FileUploadWrongCode          = 50004 // 文件上传失败，请重新上传
	DoNotRepeatCode              = 50005
	NoAuthCode                   = 50006
	CorpNotAuthCode              = 50007
	URLErrorCode                 = 50008
	FileSaveWrongCode            = 50009 // 文件保存失败，请重新上传
	AgentCanSeeCode              = 50016 //存在成员不可见
	SmsNotAuthCode               = 55000 //SmsNotAuthCode 短信相关
	UnboundEnterprise            = 41000 // 未绑定企业
	AccountsDisabled             = 42000 // 账号禁用
	EnterpriseAccountsDisabled   = 43000 // 企业账号禁用
	EnterpriseDisabled           = 44000 // 企业禁用
	EnterpriseNotExist           = 45000 // 企业不存在
	EnterpriseAccountNotExist    = 46000 // 企业账号不存在
	NoPermissionInterface        = 47000 // 无权限接口

	CasesUniqueIDNoTaskNo = 61000 // 该账号无收录
)

// error
var (
	// 用户登录授权错误
	LoginInvalidError               = NewError(LoginInvalidCode, "登录状态无效，请重新登录")
	LoginExpiredError               = NewError(LoginExpiredCode, "登录状态已过期，请重新登录")
	AccountNotExistError            = NewError(AccountNotExistCode, "账号不存在")
	AccountOrPasswordUnmatchError   = NewError(AccountOrPasswordUnmatchCode, "账号密码不匹配")
	PhoneFormatError                = NewError(PhoneFormatErrorCode, "请输入正确的手机号")
	IPRequestSmsTooMuchError        = NewError(IPRequestSmsTooMuchCode, "当前IP请求验证码太频繁, 请稍后再试")
	SliderVerifyError               = NewError(SliderVerifyCode, "滑块验证失败")
	LoginCodeError                  = NewError(LoginCodeCode, "短信验证码错误")
	PhoneNotRegError                = NewError(PhoneNotRegCode, "该手机号未注册")
	SendCodeError                   = NewError(SendCodeErrorCode, "人机校验未通过")
	AccountExistsError              = NewError(AccountExistsCode, "账号已存在")
	AccountNotExistsError           = NewError(AccountNotExistsCode, "账号不存在")
	SystemError                     = NewError(SystemErrorCode, "服务器开小差了，过会儿重试")
	ParametersInvalid               = NewError(ParametersInvalidCode, "参数有误或格式不合法")
	FileFormatIsWrong               = NewError(FileFormatIsWrongCode, "文件格式错误，请重新上传")
	FileTooLarge                    = NewError(FileTooLargeCode, "文件过大，请重新上传")
	FileEncryption                  = NewError(FileTooLargeCode, "文件为加密状态，请上传解密后的文件")
	FileUploadWrong                 = NewError(FileUploadWrongCode, "文件上传失败，请重新上传")
	DoNotRepeat                     = NewError(DoNotRepeatCode, "请勿重复或频繁操作，请稍后再试")
	NoAuth                          = NewError(NoAuthCode, "您还没有该应用的使用权限，请联系管理员授权登录")
	URLError                        = NewError(URLErrorCode, "请输入正确链接")
	FileSaveWrong                   = NewError(FileSaveWrongCode, "文件保存失败，请重新上传")
	RequestParamError               = NewError(RequestParamErrorCode, "请求参数错误")
	NoSmsAuth                       = NewError(SmsNotAuthCode, "没有操作短信营销的权限")
	UnboundEnterpriseError          = NewError(UnboundEnterprise, "未绑定企业,无法使用当前功能")
	AccountsDisabledError           = NewError(AccountsDisabled, "该账号被禁用")
	EnterpriseAccountsDisabledError = NewError(EnterpriseAccountsDisabled, "该企业账号被禁用")
	EnterpriseDisabledError         = NewError(EnterpriseDisabled, "该企业被禁用")
	EnterpriseNotExistError         = NewError(EnterpriseNotExist, "该企业不存在")
	EnterpriseAccountNotExistError  = NewError(EnterpriseAccountNotExist, "该企业账号不存在")
	MailAccountParamsError          = NewError(SystemErrorCode, "参数错误,请检查并重新输入")
	MailOpUnAllowedError            = NewError(SystemErrorCode, "操作不被允许")
	CasesUniqueIDNoTaskNoError      = NewError(CasesUniqueIDNoTaskNo, "该账号暂未收录，系统自动收录中")
)
